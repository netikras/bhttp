#!/bin/bash

## @author      netikras
## @description a simple HTTP webserver based on bash and netcat
## @home        https://gitlab.com/netikras/bhttp


## Run   me: ./example.bash 0.0.0.0 1234
## Query me: curl -siLk http://localhost:1234/index.html

. bhttp.bash



## Map multiple method:url patterns to the same handler
http::add_mapping GET /             "main"
http::add_mapping GET /index.html   "main"
main() {
    http::response 200 OK
    http::append_data "${_r}" "<html><h2>Home page</h1><br/><pre>${method} ${url}${NL}${headers}${NL}${NL}${body}</pre></html>"
    http::send "${_r}"
}

## Map different methods
http::add_mapping POST / "echo_ok"
echo_ok() {
    local method=${1}
    local url=${2}
    local headers=${3}
    local body=${4}
    http::response 200 OK
    http::append_data "${_r}" "<html><h2>Home page</h1><br/><pre>${method} ${url}${NL}${headers}${NL}${NL}${body}</pre></html>"
    http::send "${_r}"
}


## Changing mapping on-the-fly
http::add_mapping GET /shutdown     "stop_server1"
stop_server1() {
    http::response 200 OK
    http::append_data "${_r}" "Are you sure? $$"
    http::send "${_r}"
    http::add_mapping GET /shutdown     "stop_server2"
}
stop_server2() {
    http::response 200 OK
    http::append_data "${_r}" "Shutting down... $$"
    http::send "${_r}"
    ## Stopping server if needed -- just return 101
    return 101
}

## Starting the server and passing the HTTP server command (could be a function or a file)
network::start server "${1:-0.0.0.0}" "${2:-1234}" "http::server_accept"


