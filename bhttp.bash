#!/bin/bash

## @author      netikras
## @description a simple HTTP webserver based on bash and netcat
## @home        https://gitlab.com/netikras/bhttp

. net.bash

CR=$'\r'
NL=$'\n'
CRLF="${CR}${NL}"


http__VERSION="HTTP/1.0"

http__LOG_FN=

http::_log() {
    local level="${1:-INFO}"
    local message="${2:-}"

    printf "$(date "+%Y-%m-%d %H:%M:%S.%N") %s %s\n" "${level^^}" "${message}" >&2
}

http::log() {
    local level="${1:-INFO}"
    local message="${2:-}"
    local fn_name

    if [ -z "${http__LOG_FN}" ] ; then
        for fn_name in \
            "log::log" \
            "log" \
            "http::_log"; \
        do
            if declare -f "${fn_name}" >/dev/null ; then
                http__LOG_FN="${fn_name}"
                break
            fi
        done
    fi

    "${http__LOG_FN}" "${level}" "${message}"
}


http::request() {
    local method=${1:-GET}
    local url=${2:-/}
    _r="${method} ${url} ${http__VERSION}${CRLF}"
}

http::response() {
    local code=${1:-200}
    local message=${2:-}
    _r="${http__VERSION} ${code} ${message}${CRLF}"
}

http::append_header() {
    local BODY=${1:?Body missing. Cannot append header}
    local header=${2:-}
    _r="${BODY}${header//${NL}/${CRLF}}${CRLF}"
}

http::append_data() {
    local BODY=${1:?Body missing. Cannot append data}
    local data=${2:-}
    _r="${BODY}${CRLF}${data}${CRLF}"
}

http::send() {
    local BODY=${1:?Message missing. Cannot send}
    local code
    local junk
    read junk code junk <<<"${BODY}"
    http::log "INFO" "<<< ${$} ${code} ${#BODY}"
    printf "%s" "${BODY}"
}



http::get_status() {
    local BODY=${1:?Body missing. Cannot get status}
    BODY="${BODY%%${CRLF}*}"
    _r="${BODY}"
}

http::get_headers() {
    local BODY=${1:?Body missing. Cannot get headers}
    BODY="${BODY%%${CRLF}${CRLF}*}"
    BODY="${BODY#*${CRLF}}"
    _r="${BODY}"
}

http::get_data() {
    local BODY=${1:?Body missing. Cannot get data}
    BODY="${BODY#*${CRLF}${CRLF}}"
    BODY="${BODY%${CRLF}*}"
    _r="${BODY}"
}

http::read_line() {
    local BODY=${1:-}
    _r="${BODY%%${CRLF}*}"
}

http::remove_line() {
    local BODY=${1:-}
    _r=${BODY#*${CRLF}}
}


http::read_message() {
    local headers
    local data
    local message

    http::read_headers
    headers="${_r}"
    http::read_payload "${headers}"
    data="${_r}"


    message="${headers}${data}"
    http::log DEBUG "Received message len_headers=${#headers}, len_data=${#data}"
#    http::log TRACE "Received message ${headers}${data:+${data}${CRLF}}"
    _r="${message}"
}

http::read_headers() {
    while read -r line ; do
        line=${line%${CR}}
        headers="${headers}${line}${NL}"
        if [ -z "${line}" ] ; then
            break;
        fi
    done

    _r="${headers}"
}

http::read_payload() {
    local headers=${1}
    
    local h
    local content_length

    local data
    
    h="${headers,,}"
    h="${h##*content-length:}"
    if [ "${#h}" != "${#headers}" ] ; then
        read content_length <<<"${h}"
        read -r -N ${content_length} data;
    else
        h="${h/transfer-encoding/}"
        if [ "${#h}" != "${#headers}" ] ; then
            read -r data
        fi
    fi

    _r="${data}"
}


## if server -- CONNECT > receive request > send repsonse
http::server_accept() {
    http::read_message
    http::process_request "${_r}" ## STDOUT will be sent as response
}

## if client -- CONNECT > send request > receive response
http::client_send() {
    send_message
    read_message
    process_message "${_r}"
}






## override-me
send_message() {
    local OVERRIDEME="send_message"
}

## override-me
process_message() {
    local OVERRIDEME="process_message"
}

http::process_request() {
    local BODY=${1:?Body missing. Cannot process request}
    local method
    local url
    local junk

    local headers
    local data

    local ret=0

    http::get_headers "${BODY//${NL}/${CRLF}}"
    headers="${_r}"
    http::get_data "${BODY//${NL}/${CRLF}}"
    data="${_r}"

    read method url junk <<<"${BODY}"

    http::get_mapping "${method}" "${url}"
    local mapping="${_r}"
    if [ -z "${mapping}" ] ; then
        http::get_default_mapping "${method}" "${url}"
        mapping="${_r}"
    fi

    if [ -z "${mapping}" ] ; then
        mapping="http::default_mapping"
    fi;

    http::log "INFO" ">>> ${$} ${method} ${url} ${#headers}/${#data} >> ${mapping}"

    "${mapping}" "${method}" "${url}" "${headers}" "${data}"
    ret=${?}

    _r="${_r}"
    return ${ret}
}

http::default_mapping() {
    local method=${1:?Method missing. Cannot process request}
    local url=${2:?URL missing. Cannot process request}
    local headers=${3:-}
    local body=${4:-}

    http::log WARNING "Mapping not found. Cannot process request [${method}: ${url}]"
    http::response 404 "Not Found"
    http::append_data "${_r}" "Page [${method}:${url}] not found"
    http::send "${_r}"
}



## Request mapping
declare -A http__MAPPING_GET
declare -A http__MAPPING_POST
declare -A http__MAPPING_PUT
declare -A http__MAPPING_DELETE
declare -A http__MAPPING_HEAD
declare -A http__MAPPING_OPTIONS
declare -A http__MAPPING_TRACE
declare -A http__MAPPING_CONNECT


http__MAPPING_GET[_]="http::default_mapping"
http__MAPPING_POST[_]="http::default_mapping"
http__MAPPING_PUT[_]="http::default_mapping"
http__MAPPING_DELETE[_]="http::default_mapping"
http__MAPPING_HEAD[_]="http::default_mapping"
http__MAPPING_OPTIONS[_]="http::default_mapping"
http__MAPPING_TRACE[_]="http::default_mapping"
http__MAPPING_CONNECT[_]="http::default_mapping"


http::get_mappings_var() {
    local method=${1:?Method missing}
    local array_name="http__MAPPING_${method}"
    _r="${array_name}"
}


http::get_default_mapping() {
    local method=${1:?Method missing}
    local url=${2:-/}

    http::get_mappings_var "${method}"
    declare -A mappings=${!_r}
    
    _r="${mappings[_]}"
}

http::get_mapping() {
    local method=${1:?Method missing}
    local url=${2:-/}

    http::get_mappings_var "${method}"

    ## Associative arrays reassignment to other variables is not possible in shell
    declare -A mappings
    eval "for key in \"\${!${_r}[@]}\" ; do mappings[\${key}]=\"\${${_r}[\${key}]}\"; done"
    _r=
    for pattern in "${!mappings[@]}" ; do
        if [[ "${url}" =~ ^${pattern}$ ]] ; then
            _r=${mappings[${pattern}]}
        fi
    done
}


http::add_mapping() {
    local method=${1:?Method missing. Cannot add mapping}
    local url_pattern=${2:?URL pattern missing. Cannot add mapping}
    local function_name=${3:?Function name missing. Cannot add mapping}

    http::get_mappings_var "${method}"
    
    local current
    eval "current=\"\${${_r}[${url_pattern}]}\""
    eval "${_r}[${url_pattern}]=\"${function_name}\""
    _r="${current}"
}



## END




