#!/bin/bash

## @author      netikras
## @description a network connection abstraction utilizing netcat to access medium
## @home        https://gitlab.com/netikras/bhttp
## @TODO        add more medium providers: socat, netcat (not nc), xinetd...

CR=$'\r'
NL=$'\n'
CRLF="${CR}${NL}"

network__KEEP_RUNNING=true

network__LOG_FN=

network::_log() {
    local level="${1:-INFO}"
    local message="${2:-}"

    printf "$(date "+%Y-%m-%d %H:%M:%S.%N") %s %s\n" "${level^^}" "${message}" >&2
}

network::log() {
    local level="${1:-INFO}"
    local message="${2:-}"
    local fn_name

    if [ -z "${network__LOG_FN}" ] ; then
        for fn_name in \
            "log::log" \
            "log" \
            "network::_log"; \
        do
            if declare -f "${fn_name}" >/dev/null ; then
                network__LOG_FN="${fn_name}"
                break
            fi
        done
    fi

    "${network__LOG_FN}" "${level}" "${message}"
}


network::make_pipe() {
    local mode=${1:?Mode not given}
    local address=${2:?Address not given}
    local port=${3:?Port not given}
    local command=${4:?Command not given}

    local fifo="/tmp/${mode}_${address}_${port}.fifo"
    rm -f "${fifo}"
    mkfifo "${fifo}"
    _r="${fifo}"
}

network::start() {
    local mode=${1:-server}
    local address=${2:?Address not given}
    local port=${3:?Port not given}
    local command=${4:?Command not given}
    local fifo
    local ret_codes=()

    local NC_PARAMS="${NC_PARAMS}"

    if [ "${mode}" = "server" ] ; then
        NC_PARAMS="${NC_PARAMS} -kl"
    fi

    network::log INFO "Starting ${mode} ${$} on ${address}:${port} with handler ${command}"
    while [ "${network__KEEP_RUNNING}" = true ] ; do

        network::make_pipe "${mode}" "${address}" "${port}" "${command}" && fifo="${_r}" && \
            < <(< "${fifo}" nc ${NC_PARAMS} "${address}" "${port}" ; network::handle_engine_code "${?}") \
            ${command} > "${fifo}"
            network::handle_cmd_code ${?}
    done
}

network::handle_engine_code() {
    local code=${1:?Engine code not given. Cannot handle}
    case ${code} in
        0) :;;
        *) network::log ERROR "Engine ${$} returned non-zero value: ${code}";;
    esac
}

network::handle_cmd_code() {
    local code=${1:?CMD code not given. Cannot handle}
    case ${code} in
        0) :;;
        101) 
            network::log INFO "Stopping ${$}..."
            network__KEEP_RUNNING=false 
            ;;
        *) network::log WARN "Handler ${$} returned non-zero value: ${code}";;
    esac
}




