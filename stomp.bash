#!/bin/bash

## @author      netikras
## @description a STOMP 1.2 client/server implementation (server is WIP)
## @home        https://gitlab.com/netikras/bhttp


. net.bash

CR=$'\r'
NL=$'\n'
EOL=${NL}
ZERO="\x00"



stomp::log() {
    network::log "${@}"
}

stomp::send() {
    local frame=${1:?Frame missing. Cannot send}
    stomp::log INFO "<<< ${$} ${#frame}"
    stomp::log TRACE "<<<${NL}${frame}"
    echo -ne "${frame}"
}


stomp::accept() {
	stomp::read_frame
	stomp::process_frame "${_r}"
}

stomp::connect() {
    local username=${1:-}
    local password=${2:-}
    local ret

#    stomp::disconnect

    stomp::message CONNECT
    if [ -n "${username}" ] ; then
        stomp::add_header "${_r}" "login:${username}"
    fi
    if [ -n "${password}" ] ; then
        stomp::add_header "${_r}" "passcode:${password}"
    fi
	stomp::add_body "${_r}" ""
    stomp::send "${_r}"

    stomp::read_frame
	stomp::process_frame "${_r}"
    ret=${?}

    stomp::disconnect
    return ${ret}
}

stomp::disconnect() {
    stomp::message DISCONNECT
    stomp::add_body "${_r}" ""
    stomp::send "${_r}"
}

stomp::read_frame() {
    local timeout=${1:-}
	local frame;
    local command
    local headers
    local body

	READ_TIMEOUT=${timeout} stomp::read_command || return 1
    command=${_r}
	frame="${_r}"

	READ_TIMEOUT=${timeout} stomp::read_headers "${_r}" || return 1
    headers=${_r}
	frame="${frame}${_r:+${NL}${_r}}"

	READ_TIMEOUT=${timeout} stomp::read_body "${_r}" || return 1
    body=${_r}
	frame="${frame}${NL}${NL}${_r}"

    stomp::log INFO ">>> ${command} ${#headers} ${#body}"
    stomp::log TRACE ">>>${NL}${frame}"
	_r=${frame}
}

stomp::get_command() {
    local frame=${1:?Frame missing. Cannot get COMMAND}
    local command
    command=${frame%%${NL}*}

    _r=${command^^}
}

stomp::get_headers() {
    local frame=${1:?Frame missing. Cannot get COMMAND}
    local headers

    headers="${frame%%${NL}${NL}*}${NL}"
    headers=${headers#*${NL}} 
    _r=${headers%${NL}}
}

stomp::get_body() { ## FIXME account for newlines in body. Account for content-length header
    local frame=${1:?Frame missing. Cannot get COMMAND}
    local body

    body=${frame##*${NL}${NL}}
    _r=${body}
}


stomp::process_frame() {
	local frame=${1:?Frame missing. Cannot process}
    local ret
    local command
    local headers
    local body

    stomp::get_command "${frame}"
    command=${_r}

    stomp::get_headers "${frame}"
    headers=${_r}

    stomp::get_body "${frame}"
    body=${_r}
	
    stomp::get_mapping "${command}" "${headers}"
    local mapping=${_r}

    if [ -z "${mapping}" ] ; then
        stomp::get_default_mapping "${command}" "${headers}"
        mapping=${_r}
    fi

    if [ -z "${mapping}" ] ; then
        mapping="stomp::default_mapping"
    fi

    stomp::log INFO "${$} ${command} ${#headers}/${#body} >> ${mapping}"

    "${mapping}" "${command}" "${headers}" "${body}"
    ret=${?}

    _r=${_r}
    return ${ret}

}

stomp::get_mappings_var() {
    local command=${1:?Command missing. Cannot get mappings var}
    local array_name="stomp__MAPPING_${command^^}"
    _r=${array_name}
}

stomp::get_default_mapping() {
    local command=${1:?Command not given. Cannoe get default mapping}
    local headers=${2:-}

    stomp::get_mappings_var "${command}"
    declare -A mappings=${!_r}
    _r="${mappings[_]}"
}

stomp::default_mapping() {
    local command=${1:?Command not given. Cannot process request}
    local headers=${2:-}

    stomp::message ERROR
    stomp::add_header "${_r}"
    stomp::add_body "${_r}"

    stomp::send "${_r}"
}

stomp::find_header() {
    local headers=${1:-}
    local target=${2:?Header being searched for missing. Cannot find header}

    while IFS=":${NL}" read -r name value ; do
        [ "${name,,}" = "${target,,}" ] && {
            _r=${value}
            return 0
        }
    done <<< "${headers}"
    _r=
    return 1
}

stomp::get_request_id() {
    local command=${1:?Command missing. Cannot get mapping}
    local headers=${2:-}
    local id
    _r=

    case "${command^^}" in
        CONNECT) 
                    stomp::find_header "${headers}" login && id="${_r}" && \
                    stomp::find_header "${headers}" passcode && id="${id}:${_r}" ;;
        CONNECTED)  stomp::find_header "${headers}" version && id=${_r} ;;
        SEND)       stomp::find_header "${headers}" destination && id=${_r} ;;
        SUBSCRIBE)  stomp::find_header "${headers}" destination && id=${_r} ;;
        UNSUBSCRIBE)stomp::find_header "${headers}" id && id=${_r} ;;
        ACK)        stomp::find_header "${headers}" id && id=${_r} ;; 
        NACK)       stomp::find_header "${headers}" id && id=${_r} ;;
        BEGIN)      stomp::find_header "${headers}" transaction && id=${_r} ;;
        COMMIT)     stomp::find_header "${headers}" transaction && id=${_r} ;;
        ABORT)      stomp::find_header "${headers}" transaction && id=${_r} ;;
        DISCONNECT) :;;
        MESSAGE)    
                    stomp::find_header "${headers}" subscription && id=${_r} || {
                        stomp::find_header "${headers}" destination && id=${_r} 
                    } ;;
        RECEIPT)    stomp::find_header "${headers}" receipt-id && id=${_r} ;;
        ERROR)      :;;
        *)          :;;
    esac
    _r=${id}
}

stomp::get_mapping() {
    local command=${1:?Command missing. Cannot get mapping}
    local headers=${2:-}

    stomp::get_mappings_var "${command}"

    ## Associative arrays reassignment to other variables is not possible in shell
    declare -A mappings
    eval "for key in \"\${!${_r}[@]}\" ; do mappings[\${key}]=\"\${${_r}[\${key}]}\"; done"
    local request_id
    stomp::get_request_id "${command}" "${headers}"
    request_id=${_r:-_}
    _r=
    for pattern in "${!mappings[@]}" ; do
        if [[ "${request_id}" =~ ^${pattern}$ ]] ; then
            _r=${mappings[${pattern}]}
        fi
    done
}

stomp::add_mapping() {
    local command=${1:?Command missing. Cannot add mapping}
    local id=${2:?ID pattern missing. Cannot add mapping}
    local function_name=${3:?Function name missing. Cannot add mapping}

    stomp::get_mappings_var "${command}"
    
    local current
    eval "current=\"\${${_r}[${id}]}\""
    eval "${_r}[${id}]=\"${function_name}\""
    _r="${current}"
}


stomp::read_command() {
    local cmd
    local ret
    read ${READ_TIMEOUT:+-t ${READ_TIMEOUT}} -r cmd
    ret=${?}
    _r=${cmd}
    return ${ret}
}

stomp::read_headers() {
    local command=${1:-}
    local line
    local headers
    local ret

    while read ${READ_TIMEOUT:+-t ${READ_TIMEOUT}} -r line ; do
        line=${line%${CR}}
        if [ -z "${line}" ] ; then
            break
        fi
        headers="${headers}${NL}${line}"
    done

    ret=${?}

    headers=${headers#${NL}}
    _r=${headers}
    return ${ret}
}

stomp::read_body() {
    local headers=${1:-}
    local body
    local content_length

	stomp::find_header "${headers}" content-length && {
		content_length=${_r}
	}
    if [ -n "${content_length}" ] ; then
        read ${READ_TIMEOUT:+-t ${READ_TIMEOUT}} -r -N ${content_length} body;
    else
        IFS="" read ${READ_TIMEOUT:+-t ${READ_TIMEOUT}} -r -d "" body
    fi
    ret=${?}
    _r=${body}
    return ${ret}
}


stomp::message() {
    local type=${1:?Message type not given}
    _r="${type^^}${EOL}"
}

stomp::add_header() {
    local message=${1:?Message [COMMAND] missing. Cannot append headers}
    local header=${2:-}
    _r="${message}${header}${EOL}"
}

stomp::add_body() {
    local message=${1:?Message missing. Cannot append headers}
    local body=${2:-}
    local headers
    if [ -n "${body}" ] ; then
        headers="content-length:${#body}${NL}"
    fi
    _r="${message}${headers}${NL}${body}${ZERO}"
}

declare -A stomp__MAPPING_
declare -A stomp__MAPPING_CONNECT
declare -A stomp__MAPPING_SEND
declare -A stomp__MAPPING_SUBSCRIBE
declare -A stomp__MAPPING_UNSUBSCRIBE
declare -A stomp__MAPPING_BEGIN
declare -A stomp__MAPPING_COMMIT
declare -A stomp__MAPPING_ABORT
declare -A stomp__MAPPING_ACK
declare -A stomp__MAPPING_NACK
declare -A stomp__MAPPING_DISCONNECT

declare -A stomp__MAPPING_CONNECTED
declare -A stomp__MAPPING_MESSAGE
declare -A stomp__MAPPING_RECEIPT
declare -A stomp__MAPPING_ERROR

stomp__MAPPING_[_]="stomp::default_mapping"
stomp__MAPPING_CONNECT[_]="stomp::default_mapping"
stomp__MAPPING_SEND[_]="stomp::default_mapping"
stomp__MAPPING_SUBSCRIBE[_]="stomp::default_mapping"
stomp__MAPPING_UNSUBSCRIBE[_]="stomp::default_mapping"
stomp__MAPPING_BEGIN[_]="stomp::default_mapping"
stomp__MAPPING_COMMIT[_]="stomp::default_mapping"
stomp__MAPPING_ABORT[_]="stomp::default_mapping"
stomp__MAPPING_ACK[_]="stomp::default_mapping"
stomp__MAPPING_NACK[_]="stomp::default_mapping"
stomp__MAPPING_DISCONNECT[_]="stomp::default_mapping"

stomp__MAPPING_CONNECTED[_]="stomp::default_mapping"
stomp__MAPPING_MESSAGE[_]="stomp::default_mapping"
stomp__MAPPING_RECEIPT[_]="stomp::default_mapping"
stomp__MAPPING_ERROR[_]="stomp::default_mapping"

PARAMS="
https://stomp.github.io/stomp-specification-1.2.html

CONNECT or STOMP
	REQUIRED: accept-version, host
	OPTIONAL: login, passcode, heart-beat
CONNECTED
	REQUIRED: version
	OPTIONAL: session, server, heart-beat
SEND
	REQUIRED: destination
	OPTIONAL: transaction
SUBSCRIBE
	REQUIRED: destination, id
	OPTIONAL: ack
UNSUBSCRIBE
	REQUIRED: id
	OPTIONAL: none
ACK or NACK
	REQUIRED: id
	OPTIONAL: transaction
BEGIN or COMMIT or ABORT
	REQUIRED: transaction
	OPTIONAL: none
DISCONNECT
	REQUIRED: none
	OPTIONAL: receipt
MESSAGE
	REQUIRED: destination, message-id, subscription
	OPTIONAL: ack
RECEIPT
	REQUIRED: receipt-id
	OPTIONAL: none
ERROR
	REQUIRED: none
	OPTIONAL: message
"

