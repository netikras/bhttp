#!/bin/bash

. stomp.bash


stomp::add_mapping CONNECT "admin:admin" logged_in
logged_in() {
    stomp::message CONNECTED
    stomp::add_header "${_r}" "version:1.2"
    stomp::add_body "${_r}" ""
    stomp::send "${_r}"
}


stomp::add_mapping "SEND" "going.nowhere" "accept_message"
accept_message() {
    local command=${1:?Command missing}
    local headers=${2:-}
    local body=${3:-}
    stomp::log DEBUG "Received body: ${body}"
}


stomp::add_mapping ERROR ".*" on_error
on_error() {
    local command=${1:?Command missing}
    local headers=${2:-}
    local body=${3:-}

    stomp::log ERROR "${command}${NL}${headers}${NL}${body:+${NL}${body}}"
    return 1
}



stomp::add_mapping CONNECTED ".*" on_connect
on_connect() {
    local command=${1:?Command missing}
    local headers=${2:-}
    local body=${3:-}

    printf "%s\n%s\n%s\n" "${command}" "${headers}" "${body}" >&2
    action="SEND" ## SEND|SUBSCRIBE
#    action=SUBSCRIBE
    queue="going.nowhere"
    message="Hi there!"
    case "${action}" in
        SEND)
            stomp::message SEND
            stomp::add_header "${_r}" "destination:${queue}"
            stomp::add_body "${_r}" "${message}"
            stomp::send "${_r}"

            stomp::read_frame
            stomp::log DEBUG "${_r}"
            ;;
        SUBSCRIBE)
            stomp::message SUBSCRIBE
            stomp::add_header "${_r}" "destination:${queue}"
            stomp::add_header "${_r}" "id:abc123"
            stomp::add_body "${_r}" ""
            stomp::send "${_r}"

            stomp::read_frame
            stomp::log DEBUG "${_r}"
            :;;
        *) :;;
    esac

    return 101
}

if [ "${1}" = "-c" ] ; then
    username="admin"
    password="admin"
    network::start client "localhost" "61613" "stomp::connect ${username} ${password}"
else
    network::start server "localhost" "8162" "stomp::accept"
fi

