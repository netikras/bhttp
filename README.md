It's a very simple HTTP webserver written in BASH.

Dependencies:
  - bash
  - nc
  - mkfifo
  - rm
  - date (for default logger)


Due to netcat limitations only single request can be handled at a time. Not enough to provide a service but should be OK for something dead simple, like exposing APIs for your own tools or so


Example:

see example.bash


For logging make either of the following functions available: (log::log|log) that accept loglevel as $1 and message as $2. If none are defined a default http::_log logger will be used
